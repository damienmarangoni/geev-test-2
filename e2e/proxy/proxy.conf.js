const PROXY_CONFIG = {
  '/products': {
    target: true,
    router: function (req) {
      var target = 'http://localhost:4200/assets/mock';
      console.log (target);
      return target;
    },
    changeOrigin: true,
    secure: false,
  },
  '/items': {
    target: true,
    router: function (req) {
      var target = 'http://localhost:4200/assets/mock';
      console.log (target);
      return target;
    },
    changeOrigin: true,
    secure: false,
  },
};

module.exports = PROXY_CONFIG;
