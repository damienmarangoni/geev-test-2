import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';

// tslint:disable-next-line: max-line-length
import {
    GeevService,
    Products,
    ProductsAds
} from '../_modules/_swagger-services-module';
import { addProductsData } from '../store/actions/products.actions';

@Injectable({
    providedIn: 'root'
})
export class AppInitService {
    constructor(
        private readonly geevService: GeevService,
        private readonly store: Store<{ products: Array<ProductsAds> }>
    ) {}

    async Init() {
        // Load first products page before app init
        return new Promise<void>(resolve => {
            this.geevService
                .getProductsList(
                    1,
                    58,
                    '44.808219099999995,-0.6078408',
                    50000,
                    'donation',
                    false,
                    'summary',
                    'creation'
                )
                .subscribe(products => {
                    this.store.dispatch(
                        addProductsData({ products: products.ads })
                    );
                    resolve();
                });
        });
    }
}
