import { TestBed, async } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { AppComponent } from './app.component';
import { GeolocService } from './_modules/app-products-management-services/services/geoloc.service';
import { Store, StoreModule } from '@ngrx/store';
import { CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { GeevService } from './_modules/_swagger-services-module';
import { of } from 'rxjs';
import { productsReducer } from './store/reducers/products.reducer';
import { ProductsPageComponent } from './_modules/app-products-management/pages/products-page/products-page.component';

describe('AppComponent', () => {
    beforeEach(async(() => {
        class GeevServiceMock {
            getProductsList() {
                return of({ productsList: [{}] });
            }
        }

        class StoreMock {
            dispatch() {}
            select() {
                return of({ productsList: [{}] });
            }
        }
        TestBed.configureTestingModule({
            imports: [RouterTestingModule],
            providers: [
                { provide: GeevService, useClass: GeevServiceMock },
                { provide: Store, useClass: StoreMock }
            ],
            declarations: [AppComponent, ProductsPageComponent],
            schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA]
        }).compileComponents();
    }));

    it('should create the app', () => {
        const fixture = TestBed.createComponent(AppComponent);
        const app = fixture.debugElement.componentInstance;
        expect(app).toBeTruthy();
    });

    it(`should have as title 'geev'`, () => {
        const fixture = TestBed.createComponent(AppComponent);
        const app = fixture.debugElement.componentInstance;
        expect(app.title).toEqual('geev');
    });
});
