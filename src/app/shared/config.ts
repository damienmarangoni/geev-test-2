export const PRODUCTS_PER_PAGE = 20;
export const TITLE_LENGTH = 25;

export const MY_GEOLOC = { longitude: -0.57, latitude: 44.8 };
