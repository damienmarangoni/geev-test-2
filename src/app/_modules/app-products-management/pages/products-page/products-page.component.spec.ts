import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductsPageComponent } from './products-page.component';
import { CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { GeevService } from 'src/app/_modules/_swagger-services-module';
import { Store } from '@ngrx/store';
import { of } from 'rxjs';
import { ProductComponent } from '../../components/product/product.component';

describe('ProductsPageComponent', () => {
    let component: ProductsPageComponent;
    let fixture: ComponentFixture<ProductsPageComponent>;

    class GeevServiceMock {
        getProductsList() {
            return of({
                page: 1,
                page_count: 48,
                ads: [
                    {
                        _id: '5dee2a2b3396700019ae47ab',
                        title: 'TEST PARSER',
                        type: 'donation',
                        category: 'miscellaneous',
                        last_update_timestamp: 1575889451,
                        closed: false,
                        reserved: false,
                        given: false,
                        acquired: false,
                        author: {
                            _id: '5dc28114064c000012bc22aa',
                            first_name: 'Clem',
                            last_name: 'Test33',
                            picture: '5dc28111064c000012bc22a9',
                            language: 'fr'
                        },
                        favorite: false,
                        creation_timestamp: 1575889451,
                        validation_timestamp: 1575889451,
                        pictures: ['5dee2a293396700019ae47aa'],
                        unlocked: false,
                        location: {
                            longitude: -0.5782194647839178,
                            latitude: 44.85587785967698,
                            obfuscated: true,
                            radius: 1000
                        },
                        unlocked_counter: 0
                    }
                ]
            });
        }
    }
    class StoreMock {
        dispatch() {}
        select() {
            return of({
                page: 1,
                page_count: 48,
                ads: [
                    {
                        _id: '5dee2a2b3396700019ae47ab',
                        title: 'TEST PARSER',
                        type: 'donation',
                        category: 'miscellaneous',
                        last_update_timestamp: 1575889451,
                        closed: false,
                        reserved: false,
                        given: false,
                        acquired: false,
                        author: {
                            _id: '5dc28114064c000012bc22aa',
                            first_name: 'Clem',
                            last_name: 'Test33',
                            picture: '5dc28111064c000012bc22a9',
                            language: 'fr'
                        },
                        favorite: false,
                        creation_timestamp: 1575889451,
                        validation_timestamp: 1575889451,
                        pictures: ['5dee2a293396700019ae47aa'],
                        unlocked: false,
                        location: {
                            longitude: -0.5782194647839178,
                            latitude: 44.85587785967698,
                            obfuscated: true,
                            radius: 1000
                        },
                        unlocked_counter: 0
                    }
                ]
            });
        }
    }

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [ProductsPageComponent, ProductComponent],
            providers: [
                { provide: GeevService, useClass: GeevServiceMock },
                { provide: Store, useClass: StoreMock }
            ],
            schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(ProductsPageComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('should fetch next page', () => {
        const service: GeevService = TestBed.get(GeevService);

        component.fullProductsList = [{ _id: 1 }];
        component.lastDisplayedProductId = 1;

        const serviceSpy = spyOn(service, 'getProductsList').and.callThrough();

        component.fetchNextPage();

        expect(component.fullProductsList.length).toEqual(1);
        expect(serviceSpy).toHaveBeenCalledTimes(1);
    });
});
