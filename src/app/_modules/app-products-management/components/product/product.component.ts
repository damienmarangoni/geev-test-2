import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';

// tslint:disable-next-line: max-line-length
import { GeolocService } from '../../../../_modules/app-products-management-services/services/geoloc.service';
import { MY_GEOLOC, TITLE_LENGTH } from '../../../../shared/config';
import { ProductsAds } from '../../../_swagger-services-module';
import { Route } from '@angular/compiler/src/core';

@Component({
    selector: 'app-product',
    templateUrl: './product.component.html',
    styleUrls: ['./product.component.scss']
})
export class ProductComponent implements OnInit {
    @Input() product: ProductsAds;
    @Input() lastProductId: number;

    // tslint:disable-next-line: no-output-on-prefix
    @Output() readonly onLastProductLoad: EventEmitter<
        boolean
    > = new EventEmitter<boolean>();

    title: string;

    quantity = 0;

    image: string;
    avatar: string;

    distance: number;
    time: number;

    unity: string;

    constructor(
        private readonly geolocServices: GeolocService,
        private readonly router: Router
    ) {}

    ngOnInit() {
        !!this.product.title && this.product.title.length > TITLE_LENGTH
            ? (this.title = `${this.product.title.slice(0, TITLE_LENGTH)}...`)
            : (this.title = `${this.product.title}`);
        // tslint:disable-next-line: max-line-length
        this.image = `https:\/\/stage-images.geev.fr/${this.product.pictures[0]}/squares/300`;
        // tslint:disable-next-line: max-line-length
        this.avatar = `https:\/\/stage-images.geev.fr/${this.product.author.picture}/squares/32`;
        this.distance = this.geolocServices.distance(
            MY_GEOLOC.latitude,
            MY_GEOLOC.longitude,
            this.product.location.latitude,
            this.product.location.longitude
        );
        this.time = this.geolocServices.time(this.distance);

        if (this.time >= 60) {
            this.time = Math.round(this.time / 60);
            this.unity = 'h';
        } else {
            this.unity = 'm';
        }
    }

    onLoadImage() {
        if (this.product._id === this.lastProductId) {
            this.onLastProductLoad.emit(true);
        }
    }

    navigateTo() {
        this.router.navigate([`products/details/${this.product._id}`]);
    }
}
