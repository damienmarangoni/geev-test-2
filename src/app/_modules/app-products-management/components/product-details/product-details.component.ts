import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { ProductsAds } from '../../../_swagger-services-module';
import { Subscription } from 'rxjs';
import { map } from 'rxjs/operators';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-product-details',
  templateUrl: './product-details.component.html',
  styleUrls: ['./product-details.component.scss']
})
export class ProductDetailsComponent implements OnInit {

  sub: Subscription;
  subStore: Subscription;

  productId: string;
  productDetails: ProductsAds;

  constructor(
    private readonly store: Store<{
        products: Array<ProductsAds>;
    }>,
    private readonly route: ActivatedRoute) { }

  ngOnInit() {

    this.sub = this.route.params.subscribe(params => {

      this.productId = params.productId;
    });
  }

  ngAfterContentInit() {
    this.subStore = this.store
            .select('products')
            .pipe(map(products => (products as any).productsList))
            .subscribe(productsList => {
              productsList.forEach(element => {
                if (element._id === this.productId) {
                  this.productDetails = element;
                }
              });
            });
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
    this.subStore.unsubscribe();
  }

}
