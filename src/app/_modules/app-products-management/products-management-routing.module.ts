import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProductsPageComponent } from './pages/products-page/products-page.component';
import { ProductDetailsComponent } from './components/product-details/product-details.component';

export const routes: Routes = [
  {
      path: '',
      children: [
          {
              path: '',
              component: ProductsPageComponent
          },
          {
              path: 'details/:productId',
              component: ProductDetailsComponent
          },
          {
              path: '**',
              redirectTo: ''
          }
        ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProductsManagementRoutingModule { }
