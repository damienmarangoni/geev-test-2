import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProductsManagementRoutingModule } from './products-management-routing.module';
import { ProductsPageComponent } from './pages/products-page/products-page.component';
import { ProductDetailsComponent } from './components/product-details/product-details.component';
import { ProductComponent } from './components/product/product.component';
import { LazyLoadImageModule } from 'ng-lazyload-image';
import { AppMaterialModule } from '../app-material/app-material.module';

@NgModule({
    declarations: [
        ProductsPageComponent,
        ProductComponent,
        ProductDetailsComponent
    ],
    imports: [
        CommonModule,
        ProductsManagementRoutingModule,
        LazyLoadImageModule,
        AppMaterialModule
    ]
})
export class ProductsManagementModule {}
