import { CommonModule } from '@angular/common';
import { ModuleWithProviders, NgModule } from '@angular/core';

// tslint:disable-next-line: max-line-length
import { GeolocService } from '../app-products-management-services/services/geoloc.service';

@NgModule({
    declarations: [],
    imports: [CommonModule]
})
export class AppProductsManagementServicesModule {
    static forRoot(): ModuleWithProviders {
        return {
            ngModule: AppProductsManagementServicesModule,
            providers: [GeolocService]
        };
    }
}
