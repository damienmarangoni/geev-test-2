import { Injectable } from '@angular/core';

// @Injectable({
//     providedIn: 'root'
// })
@Injectable()
export class GeolocService {
    constructor() {}

    convertRad(input) {
        return (Math.PI * input) / 180;
    }

    distance(lat_a_degre, lon_a_degre, lat_b_degre, lon_b_degre) {
        const R = 6378000;

        const lat_a = this.convertRad(lat_a_degre);
        const lon_a = this.convertRad(lon_a_degre);
        const lat_b = this.convertRad(lat_b_degre);
        const lon_b = this.convertRad(lon_b_degre);

        const d =
            R *
            (Math.PI / 2 -
                Math.asin(
                    Math.sin(lat_b) * Math.sin(lat_a) +
                        Math.cos(lon_b - lon_a) *
                            Math.cos(lat_b) *
                            Math.cos(lat_a)
                ));

        return Math.round(d / 1000);
    }

    time(d) {
        return d * 2; // Minutes
    }
}
