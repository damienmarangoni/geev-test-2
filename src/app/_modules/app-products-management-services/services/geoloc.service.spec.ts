import { TestBed } from '@angular/core/testing';

import { GeolocService } from './geoloc.service';

describe('GeolocService', () => {
    beforeEach(() =>
        TestBed.configureTestingModule({
            providers: [GeolocService]
        })
    );

    it('should be created', () => {
        const service: GeolocService = TestBed.get(GeolocService);
        expect(service).toBeTruthy();
    });

    it('should calculate distance', () => {
        const service: GeolocService = TestBed.get(GeolocService);
        expect(service.distance(-0.5, 45, -0.5, 45)).toEqual(0);
    });

    it('should calculate time', () => {
        const service: GeolocService = TestBed.get(GeolocService);
        expect(service.time(120)).toEqual(240);
    });
});
