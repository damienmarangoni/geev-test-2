import { NgModule, APP_INITIALIZER } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';

import { LazyLoadImageModule, scrollPreset } from 'ng-lazyload-image';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PageNotFoundComponent } from './pages/page-not-found/page-not-found.component';
import { ApiModule } from './_modules/_swagger-services-module/api.module';
import {
    Configuration,
    ConfigurationParameters,
    BASE_PATH
} from './_modules/_swagger-services-module';
import { environment } from '../environments/environment';
import { AppInitService } from './services/app-init.service';
import { StoreModule } from '@ngrx/store';
import { StoreRouterConnectingModule, routerReducer } from '@ngrx/router-store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { productsReducer } from './store/reducers/products.reducer';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppMaterialModule } from './_modules/app-material/app-material.module';
import { AppProductsManagementServicesModule } from './_modules/app-products-management-services/app-products-management-services.module';

export function apiConfigFactory(): Configuration {
    const params: ConfigurationParameters = {
        // set configuration parameters here.
    };

    return new Configuration(params);
}

export function initializeApp(appInitService: AppInitService) {
    return async (): Promise<any> => {
        return appInitService.Init();
    };
}

@NgModule({
    declarations: [AppComponent, PageNotFoundComponent],
    imports: [
        BrowserModule.withServerTransition({ appId: 'serverApp' }),
        AppRoutingModule,
        HttpClientModule,
        ApiModule.forRoot(apiConfigFactory),
        StoreModule.forRoot({
            products: productsReducer
        }),
        StoreRouterConnectingModule.forRoot(),
        StoreDevtoolsModule.instrument({
            maxAge: 25,
            logOnly: environment.production
        }),
        LazyLoadImageModule.forRoot({
            preset: scrollPreset
        }),
        BrowserAnimationsModule,
        AppMaterialModule,
        AppProductsManagementServicesModule.forRoot()
    ],
    entryComponents: [],
    providers: [
        AppInitService,
        {
            provide: APP_INITIALIZER,
            useFactory: initializeApp,
            deps: [AppInitService],
            multi: true
        },
        { provide: BASE_PATH, useValue: environment.API_BASE_PATH }
    ],
    bootstrap: [AppComponent]
})
export class AppModule {}
